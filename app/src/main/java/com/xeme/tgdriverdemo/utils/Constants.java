package com.xeme.tgdriverdemo.utils;

import com.firebase.geofire.GeoLocation;
import com.xeme.tgdriverdemo.R;

import java.text.SimpleDateFormat;

/**
 * Constant values reused in this sample.
 */
public final class Constants {
    public static final int SUCCESS_RESULT = 0;

    public static final int FAILURE_RESULT = 1;

    public static final String PACKAGE_NAME = "com.xeme.tgdemo2";

    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";

    public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";

    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";
    public static final int Permission_Fine_Location = 123;
    public static final int Permission_Write_external = 124;
    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 125;
    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 20000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    public static final String TAG = "TGDemo2";


    public static final String TAG2= "******Error*****";
    public static final int REQUEST_CHECK_SETTINGS = 0x1;

    //fragp2p


    /**
     * Standard activity result: operation canceled.
     */
    public static final int RESULT_CANCELED = 0;
    /**
     * Standard activity result: operation succeeded.
     */
    public static final int RESULT_OK = -1;

    public static final GeoLocation INITIAL_CENTER = new GeoLocation(22.9936, 72.5056);
    public static final String GEO_FIRE_REF = "https://tgtest.firebaseio.com/Driver/location";
    public static final String GEO_FIRE_Driver = "https://tgtest.firebaseio.com/Driver/Drivers";
    public static final int INITIAL_ZOOM_LEVEL = 15;
    public static int REQUEST_CODE_RECOVER_PLAY_SERVICES = 200;
    public static final String FIREBASE_BOOK_AT = "https://tgtest.firebaseio.com/Booking/Booking/Order";

    final public static int CarTypesdrawbleid[]={R.drawable.car_auto_rickshaw,R.drawable.car_micro,R.drawable.car_mini,R.drawable.car_sedan,R.drawable.car_hatchback,R.drawable.car_minibus};
    final public static String CarTypesdrawbleText[]={"Auto","Micro","Mini","Sedan","SUV","Mini Bus"};
    final public static String BookingTypesText[]={"Point To point","Airport Transfer","Hourly Rental","Outstation"};

    final public static SimpleDateFormat formate_date = new SimpleDateFormat("dd-MM-yyyy");
    final public static  SimpleDateFormat format_time = new SimpleDateFormat("hh:mm a");

}
