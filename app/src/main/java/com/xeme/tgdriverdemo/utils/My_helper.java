package com.xeme.tgdriverdemo.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by root on 30/5/16.
 */
public class My_helper {
    public static int result = 0;
    static Mydata m;
    Bundle b=new Bundle();

    public My_helper() {

    }

    public void showdialog(Context ctx, String Title, String msg, String Positive, String Negative, boolean isCancelable) {


        result = 0;
        new AlertDialog.Builder(ctx)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(Title)
                .setCancelable(isCancelable)
                .setMessage(msg)
                .setPositiveButton(Positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        b.putInt("ForWho",1);
                        b.putInt("Result",1);
                        BusProvider.getInstance().post(new Mydata(b));
                        BusProvider.getInstance().post("Hello from My_helper");



                    }

                })
                .setNegativeButton(Negative, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        b.putInt("ForWho",1);
                        b.putInt("Result",2);
                        BusProvider.getInstance().post(new Mydata(b));
                        BusProvider.getInstance().post("Hello from My_helper");



                    }
                })

                .show();

    }


    public class Mydata {
        public int result;
       public Bundle b;


        public Mydata(int result) {
            this.result = result;
        }

        public Mydata(Bundle b) {
            this.b = b;
        }
    }

    public static void log(String TAG,String log){

        if (!TAG.isEmpty())
        {
            Log.wtf(TAG, log);

        }
        else {
            Log.wtf(Constants.TAG, log);
        }

    }

}
