package com.xeme.tgdriverdemo;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.afollestad.assent.Assent;
import com.afollestad.assent.AssentActivity;
import com.afollestad.assent.AssentCallback;
import com.afollestad.assent.PermissionResultSet;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.squareup.otto.Subscribe;
import com.xeme.tgdriverdemo.activity.MainActivity;
import com.xeme.tgdriverdemo.utils.BusProvider;
import com.xeme.tgdriverdemo.utils.Constants;
import com.xeme.tgdriverdemo.utils.My_helper;

public class SplashScreen extends AssentActivity {
    My_helper my_helper;
    String perm[] = {Assent.ACCESS_FINE_LOCATION, Assent.WRITE_EXTERNAL_STORAGE};
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        my_helper = new My_helper();



        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {

            if (ContextCompat.checkSelfPermission(SplashScreen.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                m_Request_Permission();
            }
            else
                gotomain();
        }


    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(Constants.TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    void m_Request_Permission() {


        Assent.requestPermissions(new AssentCallback() {
            @Override
            public void onPermissionResult(PermissionResultSet result) {
                if (result.allPermissionsGranted()) {
                    gotomain();
                } else {
                    my_helper.showdialog(SplashScreen.this, "Permission Required", "Please Grant Permission To use this App", "Retry", "No Never", false);
                }

            }
        }, 69, perm[0]);
    }


    @Subscribe
    public void otto_perm_result(My_helper.Mydata m) {

        if (m.b.getInt("ForWho") == 1) {

            Toast.makeText(SplashScreen.this, "Result is" + m.b.getInt("ForWho"), Toast.LENGTH_SHORT).show();

            if (m.b.getInt("Result") == 1) {
                m_Request_Permission();

            } else if (m.b.getInt("Result") == 2) {
                this.finish();
            }
        }


    }

    @Subscribe
    public void getmsg(String s) {
        Toast.makeText(SplashScreen.this, "getmsg :" + s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);

    }

    void gotomain(){
        startActivity(new Intent(SplashScreen.this, MainActivity.class));
        finish();
    }
}
