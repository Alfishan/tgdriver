package com.xeme.tgdriverdemo.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.MutableData;
import com.firebase.client.Query;
import com.firebase.client.Transaction;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.squareup.otto.Subscribe;
import com.xeme.tgdriverdemo.MyFirebaseMessagingService;
import com.xeme.tgdriverdemo.R;
import com.xeme.tgdriverdemo.fragments.Frag_ETC;
import com.xeme.tgdriverdemo.fragments.Frag_main;
import com.xeme.tgdriverdemo.pojos.RRide_AT_pojo;
import com.xeme.tgdriverdemo.utils.BusProvider;
import com.xeme.tgdriverdemo.utils.Constants;
import com.xeme.tgdriverdemo.utils.gpslocation;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        ResultCallback<LocationSettingsResult>, View.OnClickListener, CompoundButton.OnCheckedChangeListener

{


    static long ordercount = 0, c_p2p = 0, c_at = 0, c_hr = 0, c_ot = 0;
    protected Location mCurrentLocation;
    protected GoogleApiClient mGoogleApiClient;
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    Toolbar toolbar;
    gpslocation Gploc;
    Firebase ref_order, ref_Drivers;
    Menu mMenu;
    Button btn_toolbar_count;
    Switch sw_on_work;
    List<RRide_AT_pojo> atholdpojo = new ArrayList<RRide_AT_pojo>();


    public MainActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Firebase.setAndroidContext(MainActivity.this);
        this.ref_order = new Firebase(Constants.FIREBASE_BOOK_AT);
        this.ref_Drivers = new Firebase(Constants.GEO_FIRE_Driver);
        Gploc = new gpslocation(MainActivity.this);
        /// toolbar = (Toolbar) findViewById(R.id.toolbar);
        //  setSupportActionBar(toolbar);

        // buildGoogleApiClient();

        EventBus.getDefault().register(this);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            if (b.containsKey("lat") && b.containsKey("lng")) {

                Toast.makeText(MainActivity.this, "Lat Long Received:" + b.getString("lat") + "," + b.getString("lng"), Toast.LENGTH_SHORT).show();
            }


        }

        initviews();

        getordercount();


        checkperm();


    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (gpslocation.mGoogleApiClient.isConnected()) {
            gpslocation.mGoogleApiClient.disconnect();
        }
    }

    private void getordercount() {


        Firebase ref_P2P = ref_order.child("P2P");
        Firebase ref_at = ref_order.child("AT");
        Firebase ref_HR = ref_order.child("HR");
        Firebase ref_OT = ref_order.child("OT");

        Query q = ref_at.orderByKey();


        ref_P2P.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                c_p2p = dataSnapshot.getChildrenCount();
                updatecount();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        ref_at.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                c_at = dataSnapshot.getChildrenCount();
                updatecount();

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        ref_HR.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                c_hr = dataSnapshot.getChildrenCount();
                updatecount();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        ref_OT.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                c_ot = dataSnapshot.getChildrenCount();
                updatecount();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        ref_Drivers.child("MyTexi5").child("IsActive").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean isactive = (Boolean) dataSnapshot.getValue();

                Log.wtf(Constants.TAG, "switch init  value=" + dataSnapshot.getValue().toString());

                sw_on_work.setChecked(isactive);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


    }


    void updatecount() {

        ordercount = c_p2p + c_at + c_hr + c_ot;
        btn_toolbar_count.setText(String.valueOf(ordercount));
    }

    private void initviews() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);


        btn_toolbar_count = (Button) findViewById(R.id.btn_toolbar_counts);

        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView, new Frag_main()).commit();

        sw_on_work = (Switch) findViewById(R.id.sw_on_work);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name,
                R.string.app_name);

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mDrawerToggle.syncState();


        sw_on_work.setOnCheckedChangeListener(this);
        btn_toolbar_count.setOnClickListener(this);
        mNavigationView.setNavigationItemSelectedListener(this);


    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);

        } else {

            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Exit")
                    .setCancelable(false)
                    .setMessage("Are you sure you want to close this App?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            MainActivity.this.finish();
                        }

                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    })
                    .show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        mMenu = menu;
        getMenuInflater().inflate(R.menu.main, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Fragment mFragment = null;

        int id = item.getItemId();

        if (id == R.id.nav_main) {
            mFragment = new Frag_main();
        }
        if (id == R.id.nav_etc) {
            mFragment = new Frag_ETC();
        }

        if (mFragment != null && !mFragment.isVisible()) {

            mFragmentManager.beginTransaction()
                    .replace(R.id.containerView, mFragment).commit();

        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {
            drawer.closeDrawer(GravityCompat.START);
        }


        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Constants.Permission_Fine_Location: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    Toast.makeText(MainActivity.this, "Location Granted", Toast.LENGTH_SHORT).show();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    Toast.makeText(MainActivity.this, "You must grant Permission", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);

    }

    @Override
    public void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);


    }

    @org.greenrobot.eventbus.Subscribe
    public void otto_perm(MyFirebaseMessagingService.Mydata1 m) {


        Log.wtf(Constants.TAG, "Ingreenrobot subcriber top  " + "Hey Reach me at: eb" + m.b.getString("lat") + " ," + m.b.getString("lng") + "," + m.b.getInt("ForWho"));

        if (m.b.getInt("ForWho") == 2) {
            Log.wtf(Constants.TAG, "Ingreenrobot subcriber if " + "Hey Reach me at: eb" + m.b.getString("lat") + " ," + m.b.getString("lng"));
        }
        if (m.b.getInt("ForWho") == 3) {
            Log.wtf(Constants.TAG, "Ingreenrobot subcriber if " + "Hey Reach me at: eb" + m.b.getString("lat") + " ," + m.b.getString("lng") + "," + m.b.getInt("ForWho"));


        }
    }


    @Subscribe
    public void otto_perm_result(MyFirebaseMessagingService.Mydata1 m) {
        Log.wtf(Constants.TAG, "InOTTO subcriber ");
        if (m.b.getInt("ForWho") == 2) {

            Toast.makeText(MainActivity.this, "Result is" + m.b.getInt("ForWho"), Toast.LENGTH_SHORT).show();

            m.b.getString("lat");
            m.b.getString("lng");

            Toast.makeText(MainActivity.this, "HJey Reach me at:" + m.b.getString("lat") + " ," + m.b.getString("lng"), Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.d(Constants.TAG, "All location settings are satisfied.");

                mGoogleApiClient.disconnect();
                // startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.d(Constants.TAG, "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(MainActivity.this, Constants.REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.d(Constants.TAG, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.d(Constants.TAG, "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(Constants.TAG, "IN onActivityResult RCODE" + requestCode);
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case Constants.REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.d(Constants.TAG, "User agreed to make required location settings changes.");
                        // startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.d(Constants.TAG, "User chose not to make required location settings changes.");
                        break;
                }
                break;

            case 125:

                if (resultCode == RESULT_OK) {
                    Place place = PlaceAutocomplete.getPlace(this, data);
                    Log.d(Constants.TAG, "Place: onresult" + place.getName());


                } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                    Status status = PlaceAutocomplete.getStatus(this, data);
                    // TODO: Handle the error.
                    Log.d(Constants.TAG, status.getStatusMessage());

                } else if (resultCode == RESULT_CANCELED) {
                    // The user canceled the operation.
                }

        }
    }


    void checkperm() {


        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(MainActivity.this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            //**************************
            builder.setAlwaysShow(true); //this is the key ingredient
            //**************************

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result.getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can initialize location
                            // requests here.
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(
                                        MainActivity.this, 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }


    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_toolbar_counts:

                startActivity(new Intent(MainActivity.this, RideRequests.class).putExtra("Forwho", 2));
                break;
        }
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        switch (buttonView.getId()) {

            case R.id.sw_on_work:
                updateWorkState(isChecked);
                break;

        }

    }


    void updateWorkState(boolean isactive) {

        final boolean a = isactive;
        ref_Drivers.child("MyTexi5").child("IsActive").runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {

                mutableData.setValue(a);

                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(FirebaseError firebaseError, boolean b, DataSnapshot dataSnapshot) {
                Log.wtf(Constants.TAG, "postTransaction:onComplete:" + firebaseError);
            }
        });
    }




}








