package com.xeme.tgdriverdemo.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.xeme.tgdriverdemo.R;
import com.xeme.tgdriverdemo.pojos.pojoall;
import com.xeme.tgdriverdemo.utils.Constants;
import com.xeme.tgdriverdemo.utils.GMapV2Direction;
import com.xeme.tgdriverdemo.utils.My_helper;
import com.xeme.tgdriverdemo.utils.gpslocation;

import org.greenrobot.eventbus.EventBus;
import org.w3c.dom.Document;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Ride_Tracing extends AppCompatActivity implements
        OnMapReadyCallback,
        LocationListener,
        View.OnClickListener {

    GoogleMap map;
    MapView mapView;
    Context mContext;
    pojoall myPOJO;
    LatLng mSourceLatlng, mDestinationLatlng;
    Button rtBtnStartRide,rtBtnCancelRide,rtBtnEndRide;


    LinearLayout rtLlStartRide,rtLlCdt,rtLlCancelRide,rtLlBottomPanel;
    @BindView(R.id.rt_tv_cdt_d)
    TextView rtTvCdtD;
    @BindView(R.id.rt_tv_cdt_t)
    TextView rtTvCdtT;
    @BindView(R.id.rt_tv_cdt_c)
    TextView rtTvCdtC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride__tracing);

        myPOJO = EventBus.getDefault().removeStickyEvent(pojoall.class);
        My_helper.log(Constants.TAG, myPOJO.getBookingID());
        mContext = Ride_Tracing.this;
        mapView = (MapView) findViewById(R.id.rt_mapview);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        ButterKnife.bind(Ride_Tracing.this);
        double[] origin = myPOJO.getSource_latlng();
        double[] destination = myPOJO.getDestination_latlng();
        mSourceLatlng = new LatLng(origin[0], origin[1]);
        mDestinationLatlng = new LatLng(destination[0], destination[1]);

        try {
            MapsInitializer.initialize(mContext);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onLocationChanged(Location location) {

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            map = googleMap;
            startLocationUpdates();
            gpslocation.movetolastlocation(map);
            map.getUiSettings().setMyLocationButtonEnabled(true);
            map.setMyLocationEnabled(true);


            View locationButton = ((View) findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            if (locationButton != null) {
                RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                // position on right bottom
                rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                rlp.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                rlp.setMargins(0, 0, 20, 0);
                initRoute();
                initviews();
            }

        } catch (SecurityException e) {

            e.printStackTrace();
        }

    }


    void initRoute() {


        double[] origin = myPOJO.getSource_latlng();
        double[] destination = myPOJO.getDestination_latlng();
        mSourceLatlng = new LatLng(origin[0], origin[1]);
        mDestinationLatlng = new LatLng(destination[0], destination[1]);
        LongOperation drawRoute = new LongOperation();
        drawRoute.execute();


    }

   void initviews(){


       rtBtnStartRide=(Button)findViewById(R.id.rt_btn_start_ride);
       rtBtnCancelRide=(Button)findViewById(R.id.rt_btn_cancel_ride);
       rtBtnEndRide=(Button)findViewById(R.id.rt_btn_end_ride);
       rtLlBottomPanel=(LinearLayout) findViewById(R.id.rt_ll_bottom_panel);
       rtLlCancelRide=(LinearLayout) findViewById(R.id.rt_ll_cancel_ride);
       rtLlStartRide=(LinearLayout) findViewById(R.id.rt_ll_start_ride);




       rtBtnStartRide.setOnClickListener(this);
       rtBtnCancelRide.setOnClickListener(this);
       rtBtnEndRide.setOnClickListener(this);


   }

    @Override
    public void onResume() {
        super.onResume();
        if (mapView != null) {
            mapView.onResume();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();


    }

    private void stopLocationUpdates() {
        try {

            gpslocation.mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    gpslocation.mGoogleApiClient);

            if (gpslocation.mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(
                        gpslocation.mGoogleApiClient, this);
            }
        } catch (SecurityException e) {

            e.printStackTrace();
        }
    }

    private void startLocationUpdates() {
        try {

            gpslocation.mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    gpslocation.mGoogleApiClient);

            if (gpslocation.mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.requestLocationUpdates(
                        gpslocation.mGoogleApiClient, gpslocation.mLocationRequest, this);
            }
        } catch (SecurityException e) {

            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rt_btn_start_ride:
                rtLlStartRide.setVisibility(View.GONE);
                rtLlCancelRide.setVisibility(View.VISIBLE);
                break;
            case R.id.rt_btn_cancel_ride:
                break;
            case R.id.rt_btn_end_ride:

                EventBus.getDefault().postSticky(myPOJO);
                startActivity(new Intent(mContext,  RideSummary.class));

                break;
        }
    }


    private class LongOperation extends AsyncTask<String, Void, PolylineOptions> {

        Bundle b = new Bundle();

        private PolylineOptions getDirection() {
            try {

                GMapV2Direction md = new GMapV2Direction();


                Document doc = md.getDocument(mSourceLatlng, mDestinationLatlng,
                        GMapV2Direction.MODE_DRIVING);

                /*try {

                    final String Distance = md.getDistanceText(doc);
                    final String time = md.getDurationText2(doc);
                    final float getDistanceValue = md.getDistanceValue(doc) / 1000;
                    Log.wtf(Constants.TAG, "time:" + time + " " + "Distance" + Distance);

                    EventBus.getDefault().post(new updatethings(time, Distance));
                    b.putString("Time", time);
                    b.putString("Distance", Distance);
                    b.getFloat("Distance_Value", getDistanceValue);

                } catch (Exception e) {
                    Log.wtf(Constants.TAG, "***Error***=" + e);
                }
*/

                ArrayList<LatLng> directionPoint = md.getDirection(doc);
                PolylineOptions rectLine = new PolylineOptions().width(14).color(
                        Color.parseColor("#2196F3")).geodesic(true);

                for (int i = 0; i < directionPoint.size(); i++) {
                    rectLine.add(directionPoint.get(i));
                }
                // isDirectionDrawn = true;

                return rectLine;
            } catch (Exception e) {
                ///possible error:
                ///java.lang.IllegalStateException: Error using newLatLngBounds(LatLngBounds, int): Map size can't be 0. Most likely, layout has not yet occured for the map view.  Either wait until layout has occurred or use newLatLngBounds(LatLngBounds, int, int, int) which allows you to specify the map's dimensions.
                return null;
            }

        }

        @Override
        protected PolylineOptions doInBackground(String... params) {
            PolylineOptions polylineOptions = null;
            try {
                polylineOptions = getDirection();
            } catch (Exception e) {
                Thread.interrupted();
            }
            return polylineOptions;
        }

        @Override
        protected void onPostExecute(PolylineOptions result) {
            // might want to change "executed" for the returned string passed
            // into onPostExecute() but that is upto you
            try {

                // EventBus.getDefault().post(new updatethings(b.getString("Time"),b.getString("Distance")));

                map.clear();///TODO: clean the path only..
                Marker s = map.addMarker(new MarkerOptions()
                        .position(mSourceLatlng)
                        .title("Origin :" + myPOJO.getPickUpAddress())
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                Marker d = map.addMarker(new MarkerOptions()
                        .position(mDestinationLatlng)
                        .title("Destination :" + myPOJO.getDropAddress())
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

                LatLngBounds.Builder bc = new LatLngBounds.Builder();
                bc.include(s.getPosition());
                bc.include(d.getPosition());
                //   map.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
                map.animateCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 100));

                map.addPolyline(result);
            } catch (Exception e) {

                Log.wtf(Constants.TAG, "********Error******* :" + e);

            }
            // zoomToPoints();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

}

