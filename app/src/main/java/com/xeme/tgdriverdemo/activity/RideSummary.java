package com.xeme.tgdriverdemo.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.xeme.tgdriverdemo.R;
import com.xeme.tgdriverdemo.pojos.pojoall;
import com.xeme.tgdriverdemo.utils.Constants;
import com.xeme.tgdriverdemo.utils.GMapV2Direction;

import org.greenrobot.eventbus.EventBus;
import org.w3c.dom.Document;

import java.util.ArrayList;

public class RideSummary extends AppCompatActivity implements
        OnMapReadyCallback,GoogleMap.OnMapClickListener {

    GoogleMap map;
    MapView mapView;
    Context mContext;
    LatLng msource, mdestination;
    pojoall myPOJO;
    Marker s,d;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_summary);
        mapView = (MapView) findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);

        mapView.getMapAsync(this);
        try {
            MapsInitializer.initialize(this);

        } catch (Exception e) {
            e.printStackTrace();
        }

        GoogleMapOptions mapOptions = new GoogleMapOptions();
        mapOptions.liteMode(true);

        init();


    }


    void initview(){



    }

    void init() {


        myPOJO = (pojoall) EventBus.getDefault().removeStickyEvent(pojoall.class);
        double[] origin = myPOJO.getSource_latlng();
        double[] destination = myPOJO.getDestination_latlng();
        msource = new LatLng(origin[0], origin[1]);
        mdestination = new LatLng(destination[0], destination[1]);
        LongOperation drawRoute = new LongOperation();
        drawRoute.execute();


    }

    @Override
    public void onBackPressed() {

        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Want To Go back?")
                .setCancelable(false)
                .setMessage("All Selected Detail Will be lost")
                .setPositiveButton("Ok Continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .show();

    }


    @Override
    public void onResume() {
        super.onResume();
        if (mapView != null) {
            mapView.onResume();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.getUiSettings().setScrollGesturesEnabled(false);
        map.getUiSettings().setZoomGesturesEnabled(false);
        map.getUiSettings().setAllGesturesEnabled(false);
         s = map.addMarker(new MarkerOptions()
                .position(new LatLng(23.1123412, 72.5319015))
                .title("Nearest Pick Up Location")
                .draggable(false)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

         d = map.addMarker(new MarkerOptions()
                .position(new LatLng(23.0067752, 72.5439393))
                .title("Nearest Drop Location")
                .draggable(false)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

        LatLngBounds.Builder bc = new LatLngBounds.Builder();
        bc.include(s.getPosition());
        bc.include(d.getPosition());
        map.animateCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 100));
         map.setOnMapClickListener(this);
    }


    @Override
    public void onMapClick(LatLng latLng) {
        LatLngBounds.Builder bc = new LatLngBounds.Builder();
        bc.include(msource);
        bc.include(mdestination);
        //   map.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
        map.animateCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 110));

    }

    private class LongOperation extends AsyncTask<String, Void, PolylineOptions> {

        Bundle b = new Bundle();

        private PolylineOptions getDirection() {
            try {

                GMapV2Direction md = new GMapV2Direction();


                Document doc = md.getDocument(msource, mdestination,
                        GMapV2Direction.MODE_DRIVING);

                /*try {

                    final String Distance = md.getDistanceText(doc);
                    final String time = md.getDurationText2(doc);
                    final float getDistanceValue = md.getDistanceValue(doc) / 1000;
                    Log.wtf(Constants.TAG, "time:" + time + " " + "Distance" + Distance);

                    EventBus.getDefault().post(new updatethings(time, Distance));
                    b.putString("Time", time);
                    b.putString("Distance", Distance);
                    b.getFloat("Distance_Value", getDistanceValue);

                } catch (Exception e) {
                    Log.wtf(Constants.TAG, "***Error***=" + e);
                }
*/

                ArrayList<LatLng> directionPoint = md.getDirection(doc);
                PolylineOptions rectLine = new PolylineOptions().width(14).color(
                        Color.parseColor("#2196F3")).geodesic(true);

                for (int i = 0; i < directionPoint.size(); i++) {
                    rectLine.add(directionPoint.get(i));
                }
                // isDirectionDrawn = true;

                return rectLine;
            } catch (Exception e) {
                ///possible error:
                ///java.lang.IllegalStateException: Error using newLatLngBounds(LatLngBounds, int): Map size can't be 0. Most likely, layout has not yet occured for the map view.  Either wait until layout has occurred or use newLatLngBounds(LatLngBounds, int, int, int) which allows you to specify the map's dimensions.
                return null;
            }

        }

        @Override
        protected PolylineOptions doInBackground(String... params) {
            PolylineOptions polylineOptions = null;
            try {
                polylineOptions = getDirection();
            } catch (Exception e) {
                Thread.interrupted();
            }
            return polylineOptions;
        }

        @Override
        protected void onPostExecute(PolylineOptions result) {
            // might want to change "executed" for the returned string passed
            // into onPostExecute() but that is upto you
            try {

                // EventBus.getDefault().post(new updatethings(b.getString("Time"),b.getString("Distance")));

                map.clear();///TODO: clean the path only..
                Marker s = map.addMarker(new MarkerOptions()
                        .position(msource)
                        .title("Origin :" + myPOJO.getPickUpAddress())
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                Marker d = map.addMarker(new MarkerOptions()
                        .position(mdestination)
                        .title("Destination :" + myPOJO.getDropAddress())
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

                LatLngBounds.Builder bc = new LatLngBounds.Builder();
                bc.include(s.getPosition());
                bc.include(d.getPosition());
                //   map.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
                map.animateCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 100));

                map.addPolyline(result);
            } catch (Exception e) {

                Log.wtf(Constants.TAG, "********Error******* :" + e);

            }
            // zoomToPoints();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }


}
