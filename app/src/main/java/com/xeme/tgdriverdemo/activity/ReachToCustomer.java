package com.xeme.tgdriverdemo.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.xeme.tgdriverdemo.R;
import com.xeme.tgdriverdemo.pojos.pojoall;
import com.xeme.tgdriverdemo.utils.Constants;
import com.xeme.tgdriverdemo.utils.GMapV2Direction;
import com.xeme.tgdriverdemo.utils.My_helper;
import com.xeme.tgdriverdemo.utils.gpslocation;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.ThreadMode;
import org.w3c.dom.Document;

import java.util.ArrayList;

public class ReachToCustomer extends AppCompatActivity implements
        OnMapReadyCallback,
        LocationListener,
        View.OnClickListener{

    GoogleMap map;
    MapView mapView;
    Context mContext;
    ProgressDialog pg_mapload;
    MarkerOptions m;
    LatLng mCurrentLatLng, mCustomerLatLng;
    boolean isFirst = true;
    TextView rtc_tv_cdt_d, rtc_tv_cdt_t;
    LongOperation ln;
    Button btn_arrived,btn_cancel,btn_call,btn_sms;
    pojoall myPOJO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reach_to_customer);
        mContext = ReachToCustomer.this;
        initPGDialog();
        mapView = (MapView) findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        try {
            MapsInitializer.initialize(mContext);

        } catch (Exception e) {
            e.printStackTrace();
        }

         myPOJO = (pojoall) EventBus.getDefault().removeStickyEvent(pojoall.class);
        My_helper.log("TGData", myPOJO.getPickUpAddress());
        m = new MarkerOptions();
        double[] d = new double[2];
        d = myPOJO.getSource_latlng();
        m.position(new LatLng(d[0], d[1]));
        m.title("Hey i am here");
        initview();
        mCustomerLatLng = new LatLng(d[0], d[1]);


    }

    void initview() {
        rtc_tv_cdt_d = (TextView) findViewById(R.id.rtc_tv_cdt_d);
        rtc_tv_cdt_t = (TextView) findViewById(R.id.rtc_tv_cdt_t);

        btn_arrived=(Button)findViewById(R.id.rtc_btn_arrived);
        btn_cancel=(Button)findViewById(R.id.rtc_btn_cancel);
        btn_call=(Button)findViewById(R.id.rtc_btn_call_customer);
        btn_sms=(Button)findViewById(R.id.rtc_btn_sms_customer);

        btn_arrived.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
        btn_sms.setOnClickListener(this);
        btn_call.setOnClickListener(this);

    }

    void initPGDialog() {

        pg_mapload = new ProgressDialog(mContext);
        pg_mapload.setTitle("Please Wait");
        pg_mapload.setMessage("Loading Map...");
        pg_mapload.setCancelable(false);
        pg_mapload.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        if (mapView != null) {
            mapView.onResume();

        }
        startLocationUpdates();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopLocationUpdates();
        EventBus.getDefault().unregister(this);
        mapView.onPause();


    }

    private void stopLocationUpdates() {
        try {

            gpslocation.mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    gpslocation.mGoogleApiClient);

            if (gpslocation.mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(
                        gpslocation.mGoogleApiClient, this);
            }
        } catch (SecurityException e) {

            e.printStackTrace();
        }
    }

    private void startLocationUpdates() {
        try {

            gpslocation.mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    gpslocation.mGoogleApiClient);

            if (gpslocation.mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.requestLocationUpdates(
                        gpslocation.mGoogleApiClient, gpslocation.mLocationRequest, this);
            }
        } catch (SecurityException e) {

            e.printStackTrace();
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        try {
            map = googleMap;
            startLocationUpdates();
            gpslocation.movetolastlocation(map);
            map.getUiSettings().setMyLocationButtonEnabled(true);
            map.setMyLocationEnabled(true);


            View locationButton = ((View) findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            if (locationButton != null) {
                RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                // position on right bottom
                rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                rlp.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                rlp.setMargins(0, 0, 20, 0);
            }

            pg_mapload.dismiss();

        } catch (SecurityException e) {

            e.printStackTrace();
        }
        // map.addMarker(m);
       /* double l[]=new double[2];
        LatLng n=gpslocation.getlatlong();
        l[0]=n.latitude;
        l[1]=n.longitude;
        Texi mTexi=new Texi(1992,DriverUID,"Tommy Lee",true,l,token);
        ref.child("MyTexi5").setValue(mTexi);*/

    }

    @Override
    public void onLocationChanged(Location location) {

        mCurrentLatLng = new LatLng(location.getLatitude(), location.getLongitude());
        ln = new LongOperation();

        ln.execute("");

        /*CircleOptions c = new CircleOptions();
        c.radius(location.getAccuracy()).strokeColor(Color.GREEN);
        map.addCircle(c);
*/

    }


    @Override
    public void onBackPressed() {


        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Exit")
                .setCancelable(false)
                .setMessage("Are you sure you want To Cancle? ")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            if (ln.getStatus() == AsyncTask.Status.RUNNING) {
                                ln.cancel(true);
                            }
                            if (ln.getStatus() == AsyncTask.Status.PENDING) {
                                ln.cancel(true);

                            }

                            EventBus.getDefault().unregister(this);
                            ReachToCustomer.this.finish();
                        } catch (Exception e) {

                        }

                    }

                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        })
                .show();

    }


    @org.greenrobot.eventbus.Subscribe(threadMode = ThreadMode.MAIN)
    public void runasync(String a, int b) {

    }


    @org.greenrobot.eventbus.Subscribe(threadMode = ThreadMode.MAIN)
    public void update(updatethings u) {

        Log.wtf(Constants.TAG, "inupdate CTD");
        rtc_tv_cdt_t.setText(u.time);
        rtc_tv_cdt_d.setText(u.Distance);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.rtc_btn_arrived:
                EventBus.getDefault().postSticky(myPOJO);
                startActivity(new Intent(mContext,  Ride_Tracing.class));
                finishActivity(1);
                break;
            case R.id.rtc_btn_cancel:
                break;
            case R.id.rtc_btn_call_customer:
                break;
            case R.id.rtc_btn_sms_customer:
                break;

        }
    }

    private class LongOperation extends AsyncTask<String, Void, PolylineOptions> {

        Bundle b = new Bundle();

        private PolylineOptions getDirection() {
            try {

                GMapV2Direction md = new GMapV2Direction();

                Document doc = md.getDocument(mCurrentLatLng, mCustomerLatLng,
                        GMapV2Direction.MODE_DRIVING);

                try {

                    final String Distance = md.getDistanceText(doc);
                    final String time = md.getDurationText2(doc);
                    final float getDistanceValue = md.getDistanceValue(doc) / 1000;
                    Log.wtf(Constants.TAG, "time:" + time + " " + "Distance" + Distance);

                    EventBus.getDefault().post(new updatethings(time, Distance));
                    b.putString("Time", time);
                    b.putString("Distance", Distance);
                    b.getFloat("Distance_Value", getDistanceValue);

                } catch (Exception e) {
                    Log.wtf(Constants.TAG, "***Error***=" + e);
                }


                ArrayList<LatLng> directionPoint = md.getDirection(doc);
                PolylineOptions rectLine = new PolylineOptions().width(14).color(
                        Color.parseColor("#2196F3")).geodesic(true);

                for (int i = 0; i < directionPoint.size(); i++) {
                    rectLine.add(directionPoint.get(i));
                }
                // isDirectionDrawn = true;

                return rectLine;
            } catch (Exception e) {
                ///possible error:
                ///java.lang.IllegalStateException: Error using newLatLngBounds(LatLngBounds, int): Map size can't be 0. Most likely, layout has not yet occured for the map view.  Either wait until layout has occurred or use newLatLngBounds(LatLngBounds, int, int, int) which allows you to specify the map's dimensions.
                return null;
            }

        }

        @Override
        protected PolylineOptions doInBackground(String... params) {
            PolylineOptions polylineOptions = null;
            try {
                polylineOptions = getDirection();
            } catch (Exception e) {
                Thread.interrupted();
            }
            return polylineOptions;
        }

        @Override
        protected void onPostExecute(PolylineOptions result) {
            // might want to change "executed" for the returned string passed
            // into onPostExecute() but that is upto you
            try {

                // EventBus.getDefault().post(new updatethings(b.getString("Time"),b.getString("Distance")));

                if (isFirst) {


                    map.clear();///TODO: clean the path only..

                    Marker s = map.addMarker(new MarkerOptions()
                            .position(mCustomerLatLng)
                            .title("Hey I Am here")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

                    LatLngBounds.Builder bc = new LatLngBounds.Builder();
                    bc.include(mCurrentLatLng);
                    bc.include(s.getPosition());
                    //   map.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
                    map.animateCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 100));
                    isFirst = false;
                }
                map.addPolyline(result);
            } catch (Exception e) {

                Log.wtf(Constants.TAG, "********Error******* :" + e);

            }
            // zoomToPoints();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public class updatethings {
        String time, Distance;


        public updatethings(String time, String distance) {
            this.time = time;
            Distance = distance;
        }
    }


}
