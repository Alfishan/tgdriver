package com.xeme.tgdriverdemo.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.xeme.tgdriverdemo.R;
import com.xeme.tgdriverdemo.adapters.at_rr_adapter;
import com.xeme.tgdriverdemo.pojos.RRide_AT_pojo;
import com.xeme.tgdriverdemo.pojos.RRide_HR_pojo;
import com.xeme.tgdriverdemo.pojos.RRide_pojo;
import com.xeme.tgdriverdemo.pojos.pojoall;
import com.xeme.tgdriverdemo.utils.Constants;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

public class RideRequests extends AppCompatActivity implements View.OnClickListener {
    RecyclerView R_listview;
    Firebase ref_order, ref_Drivers, ref_P2P, ref_at, ref_HR, ref_OT;
    List<RRide_AT_pojo> atholdpojo;
    List<RRide_HR_pojo> hrholdpojo;
    List<RRide_pojo> otholdpojo;
    List<RRide_pojo> p2pholdpojo;
    at_rr_adapter adapter;
    RadioGroup rg;
    RadioButton rb_p2p, rb_at, rb_hr, rb_ot;
    at_rr_adapter rr_adapter;
    int currentAdapter;
    pojoall pojo;


    //-----dialog things
    Dialog MIDialog;
    TextView mid_tv_booking_type, mid_tv_ride_type, mid_tv_pickup_text,
            mid_tv_drop_text, mid_tv_cdt_c, mid_tv_cdt_d, mid_tv_cdt_t,
            mid_tv_time, mid_tv_date, mid_tv_Package, mid_tv_airport;
    ImageView mid_iv_ride_type;
    LinearLayout mid_ll_drop_main, mid_ll_cdt_main, mid_ll_package, mid_ll_airport;
    Button mid_btn_accept, mid_btn_cancle;

    //-----------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_requests);
        Firebase.setAndroidContext(RideRequests.this);
        this.ref_order = new Firebase(Constants.FIREBASE_BOOK_AT);
        this.ref_Drivers = new Firebase(Constants.GEO_FIRE_Driver);
        rr_adapter = new at_rr_adapter(RideRequests.this);

        init();
    }

    private void init() {
        ref_P2P = ref_order.child("P2P");
        ref_at = ref_order.child("AT");
        ref_HR = ref_order.child("HR");
        ref_OT = ref_order.child("OT");

///-----------------
        initonce();
        rg = (RadioGroup) findViewById(R.id.tl_rg);
        rb_p2p = (RadioButton) findViewById(R.id.rb_p2p);
        rb_at = (RadioButton) findViewById(R.id.rb_at);
        rb_hr = (RadioButton) findViewById(R.id.rb_hr);
        rb_ot = (RadioButton) findViewById(R.id.rb_ot);

        rb_p2p.setOnClickListener(this);
        rb_at.setOnClickListener(this);
        rb_hr.setOnClickListener(this);
        rb_ot.setOnClickListener(this);


        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {

                    case R.id.rb_p2p:
                        setadapter(1);
                        break;
                    case R.id.rb_at:
                        setadapter(2);
                        break;
                    case R.id.rb_hr:
                        setadapter(3);
                        break;
                    case R.id.rb_ot:
                        setadapter(4);
                        break;

                    default:
                        break;

                }


            }

        });

//---------------------------
        R_listview = (RecyclerView) findViewById(R.id.card_listView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        R_listview.setLayoutManager(mLayoutManager);
        R_listview.setItemAnimator(new SlideInUpAnimator());

        ref_at.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                atholdpojo = new ArrayList<RRide_AT_pojo>();


                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    atholdpojo.add(ds.getValue(RRide_AT_pojo.class));

                }
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }


            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        ref_P2P.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                p2pholdpojo = new ArrayList<RRide_pojo>();

                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    p2pholdpojo.add(ds.getValue(RRide_pojo.class));

                }

                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        ref_HR.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                hrholdpojo = new ArrayList<RRide_HR_pojo>();

                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    hrholdpojo.add(ds.getValue(RRide_HR_pojo.class));

                }

                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        ref_OT.orderByKey().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                otholdpojo = new ArrayList<RRide_pojo>();

                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    otholdpojo.add(ds.getValue(RRide_pojo.class));

                }

                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


        rr_adapter.setOnItemClickListener(new at_rr_adapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                String BookingID = "";

                if (currentAdapter == 1) {
                    BookingID = p2pholdpojo.get(position).getBookingID();
                    ShowInfo(BookingID, ref_P2P, 1);

                } else if (currentAdapter == 2) {
                    BookingID = atholdpojo.get(position).getBookingID();
                    ShowInfo(BookingID, ref_at, 2);

                } else if (currentAdapter == 3) {
                    BookingID = hrholdpojo.get(position).getBookingID();
                    ShowInfo(BookingID, ref_HR, 3);
                } else if (currentAdapter == 4) {
                    BookingID = otholdpojo.get(position).getBookingID();
                    ShowInfo(BookingID, ref_OT, 4);
                }

                Toast.makeText(RideRequests.this, BookingID, Toast.LENGTH_SHORT).show();
            }
        });


        /*Firebase f=new Firebase("https://tgtest.firebaseio.com/Booking/Booking/Order");

        Query q=f.orderByChild("AT").equalTo(2,"BookingTypeID");
        q.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Toast.makeText(RideRequests.this, "just at orders"+ dataSnapshot.getChildrenCount(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
*/

    }


    void setadapter(int a) {


        switch (a)

        {

            case 1:

                adapter = new at_rr_adapter(p2pholdpojo, null, null, 1);
                R_listview.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                currentAdapter = 1;

                break;

            case 2:
                adapter = new at_rr_adapter(null, atholdpojo, null, 2);
                R_listview.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                currentAdapter = 2;
                break;
            case 3:
                adapter = new at_rr_adapter(null, null, hrholdpojo, 3);
                R_listview.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                currentAdapter = 3;
                break;

            case 4:
                adapter = new at_rr_adapter(otholdpojo, null, null, 1);
                R_listview.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                currentAdapter = 4;
                break;


        }

    }


    @Override
    public void onClick(View v) {
        switch (rg.getCheckedRadioButtonId()) {


        }
        switch (v.getId()) {

            case R.id.mid_btn_cancle:
                if (MIDialog.isShowing()) {

                    MIDialog.dismiss();

                }
                break;


        }
    }


    void initonce() {

        ref_P2P.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                p2pholdpojo = new ArrayList<RRide_pojo>();

                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    p2pholdpojo.add(ds.getValue(RRide_pojo.class));

                }

                adapter = new at_rr_adapter(p2pholdpojo, null, null, 1);
                R_listview.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                currentAdapter = 1;

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

    void ShowInfo(final String Bookingid, final Firebase ref, int adaptype) {

        final int reftype = adaptype;

        final DataSnapshot[] ds = new DataSnapshot[1];
        MIDialog = new Dialog(RideRequests.this);
        MIDialog.getWindow().setBackgroundDrawable(null);
        MIDialog.getWindow().setGravity(Gravity.CENTER | Gravity.CENTER_VERTICAL);
        MIDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        MIDialog.setContentView(R.layout.dialog_rr_details);
        MIDialog.getWindow().getAttributes().windowAnimations = R.style.animationdialog;
        MIDialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        MIDialog.getWindow().setBackgroundDrawable(null);
        MIDialog.setCanceledOnTouchOutside(true);
        initMID(MIDialog);


        ref.child(Bookingid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ds[0] = dataSnapshot;
                pojo = new pojoall();
                pojo = dataSnapshot.getValue(pojoall.class);

                Log.wtf("TGREF", dataSnapshot.toString());
                Log.wtf("TGREF", pojo.getPickUpAddress());

                mid_tv_booking_type.setText(Constants.BookingTypesText[pojo.getBookingTypeID() - 1]);
                mid_iv_ride_type.setImageResource(Constants.CarTypesdrawbleid[pojo.getRideTypeID()]);
                mid_tv_ride_type.setText(Constants.CarTypesdrawbleText[pojo.getRideTypeID()]);
                mid_tv_pickup_text.setText(pojo.getPickUpAddress());
                mid_tv_time.setText(Constants.format_time.format(pojo.getPickUpTime()));
                mid_tv_date.setText(Constants.formate_date.format(pojo.getPickUpDate()));


                if (reftype != 3) {

                    mid_tv_drop_text.setText(pojo.getDropAddress());
                    mid_tv_cdt_c.setText(pojo.getEstimatedCost());
                    mid_tv_cdt_d.setText(pojo.getEstimatedDistance());
                    mid_tv_cdt_t.setText(pojo.getEstimatedTime());


                } else {
                    mid_ll_drop_main.setVisibility(View.GONE);
                    mid_ll_package.setVisibility(View.VISIBLE);
                    mid_tv_Package.setText(pojo.getPackageType());
                    mid_ll_cdt_main.setVisibility(View.GONE);
                }
                if (reftype == 2) {
                    mid_ll_airport.setVisibility(View.VISIBLE);
                    if (pojo.ISGoingToAirPort()) {

                        mid_tv_airport.setText("Going To Airport");
                    } else {
                        mid_tv_airport.setText("Coming  From Airport");
                    }

                }

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


        mid_btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                new AlertDialog.Builder(RideRequests.this).setTitle("Confirm Ride Request").setMessage("want to accept this request?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        EventBus.getDefault().postSticky(ds[0].getValue(pojoall.class));
                        startActivity(new Intent(RideRequests.this, ReachToCustomer.class).putExtra("BookingID", Bookingid));
                        MIDialog.dismiss();
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();


            }
        });

        MIDialog.show();


    }

    void initMID(Dialog dialog) {

        mid_btn_accept = (Button) dialog.findViewById(R.id.mid_btn_accept);
        mid_btn_cancle = (Button) dialog.findViewById(R.id.mid_btn_cancle);
        mid_btn_cancle.setOnClickListener(this);
        mid_tv_booking_type = mid_tv_ride_type = (TextView) dialog.findViewById(R.id.mid_tv_booking_type);
        mid_iv_ride_type = (ImageView) dialog.findViewById(R.id.mid_iv_ride_type);
        mid_tv_ride_type = (TextView) dialog.findViewById(R.id.mid_tv_ride_type);
        mid_tv_pickup_text = (TextView) dialog.findViewById(R.id.mid_tv_pickup_text);
        mid_tv_drop_text = (TextView) dialog.findViewById(R.id.mid_tv_drop_text);
        mid_tv_time = (TextView) dialog.findViewById(R.id.mid_tv_time);
        mid_tv_date = (TextView) dialog.findViewById(R.id.mid_tv_date);
        mid_tv_cdt_c = (TextView) dialog.findViewById(R.id.mid_tv_cdt_c);
        mid_tv_cdt_d = (TextView) dialog.findViewById(R.id.mid_tv_cdt_d);
        mid_tv_cdt_t = (TextView) dialog.findViewById(R.id.mid_tv_cdt_t);
        mid_tv_Package = (TextView) dialog.findViewById(R.id.mid_tv_Package);
        mid_tv_airport = (TextView) dialog.findViewById(R.id.mid_tv_airport);


        mid_ll_cdt_main = (LinearLayout) dialog.findViewById(R.id.mid_ll_cdt_main);
        mid_ll_airport = (LinearLayout) dialog.findViewById(R.id.mid_ll_airport);
        mid_ll_drop_main = (LinearLayout) dialog.findViewById(R.id.mid_ll_drop_main);
        mid_ll_package = (LinearLayout) dialog.findViewById(R.id.mid_ll_package);


    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {

            String result;
            String forwho = null;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    result = bundle.getString("address");
                    forwho = bundle.getString("forwho");
                    break;
                default:
                    result = null;
            }

            if (forwho != null) {
                if (forwho.equals("tvPickAddress")) {
                    //  tvPickAddress.setText(result);

                }
            } else {

                Toast.makeText(RideRequests.this, "Fail to get Address", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
