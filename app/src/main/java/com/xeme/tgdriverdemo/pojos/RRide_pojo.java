package com.xeme.tgdriverdemo.pojos;

import java.util.Date;

/**
 * Created by root on 1/6/16.
 */
public class RRide_pojo {


    String EstimatedCost;
    String BookingID;
    String EstimatedDistance;
    String EstimatedTime;
    String PickUpAddress;
    String DropAddress;
    Date PickUpTime, PickUpDate;
    Double Source_latlng[], Destination_latlng[], User_latlng[];
    int RideTypeID, BookingTypeID;

    public RRide_pojo(String BookingID,
                      int BookingTypeID,
                      int RideTypeID,
                      Double[] User_latlng,
                      Double[] Source_latlng,
                      String PickUpAddress,
                      Double[] Destination_latlng,
                      String DropAddress,
                      Date PickUpTime,
                      Date PickUpDate,
                      String EstimatedCost,
                      String EstimatedDistance,
                      String EstimatedTime) {
        this.BookingID = BookingID;
        this.BookingTypeID = BookingTypeID;
        this.RideTypeID = RideTypeID;
        this.User_latlng = User_latlng;
        this.Source_latlng = Source_latlng;
        this.PickUpAddress = PickUpAddress;
        this.Destination_latlng = Destination_latlng;
        this.DropAddress = DropAddress;
        this.PickUpTime = PickUpTime;
        this.PickUpDate = PickUpDate;
        this.EstimatedCost = EstimatedCost;
        this.EstimatedDistance = EstimatedDistance;
        this.EstimatedTime = EstimatedTime;
    }

    public RRide_pojo() {
    }

    public String getDropAddress() {
        return DropAddress;
    }

    public void setDropAddress(String dropAddress) {
        DropAddress = dropAddress;
    }

    public String getPickUpAddress() {
        return PickUpAddress;
    }

    public void setPickUpAddress(String pickUpAddress) {
        PickUpAddress = pickUpAddress;
    }

    public String getBookingID() {
        return BookingID;
    }

    public void setBookingID(String bookingId) {
        BookingID = bookingId;
    }

    public int getBookingTypeID() {
        return BookingTypeID;
    }

    public void setBookingTypeID(int bookingTypeID) {
        BookingTypeID = bookingTypeID;
    }

    public int getRideTypeID() {
        return RideTypeID;
    }

    public void setRideTypeID(int rideTypeID) {
        RideTypeID = rideTypeID;
    }

    public Double[] getUser_latlng() {
        return User_latlng;
    }

    public void setUser_latlng(Double[] user_latlng) {
        User_latlng = user_latlng;
    }

    public Double[] getSource_latlng() {
        return Source_latlng;
    }

    public void setSource_latlng(Double[] source_latlng) {
        Source_latlng = source_latlng;
    }

    public Double[] getDestination_latlng() {
        return Destination_latlng;
    }

    public void setDestination_latlng(Double[] destination_latlng) {
        Destination_latlng = destination_latlng;
    }

    public String getEstimatedCost() {
        return EstimatedCost;
    }

    public void setEstimatedCost(String estimatedCost) {
        EstimatedCost = estimatedCost;
    }

    public String getEstimatedDistance() {
        return EstimatedDistance;
    }

    public void setEstimatedDistance(String estimatedDistance) {
        EstimatedDistance = estimatedDistance;
    }

    public String getEstimatedTime() {
        return EstimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        EstimatedTime = estimatedTime;
    }

    public Date getPickUpTime() {
        return PickUpTime;
    }

    public void setPickUpTime(Date pickUpTime) {
        PickUpTime = pickUpTime;
    }

    public Date getPickUpDate() {
        return PickUpDate;
    }

    public void setPickUpDate(Date pickUpDate) {
        PickUpDate = pickUpDate;
    }

}
