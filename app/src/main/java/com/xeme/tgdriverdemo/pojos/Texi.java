package com.xeme.tgdriverdemo.pojos;

/**
 * Created by root on 30/5/16.
 */
public class Texi {

    int TexiUID;
    int DriverUID;
    String DriverName;
    String Token;
    boolean IsActive;
    double l[];

    public Texi() {
    }

    public Texi(int TexiUID, String DriverName, boolean isActive, double[] l) {
        this.TexiUID = TexiUID;
        this.DriverName = DriverName;
        IsActive = isActive;
        this.l = l;
    }
    public Texi(int TexiUID,int DriverUID, String DriverName, boolean IsActive, double[] l, String Token) {
        this.TexiUID = TexiUID;
        this.DriverName = DriverName;
        this. IsActive = IsActive;
        this.l = l;
        this.Token = Token;
        this.DriverUID=DriverUID;
    }

    public int getDriverUID() {
        return DriverUID;
    }

    public void setDriverUID(int driverUID) {
        DriverUID = driverUID;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        this.Token = token;
    }

    public int getTexiUID() {
        return TexiUID;
    }

    public void setTexiUID(int TexiUID) {
        this.TexiUID = TexiUID;
    }

    public String getDriverName() {
        return DriverName;
    }

    public void setDriverName(String DriverName) {
        this.DriverName = DriverName;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean IsActive) {
        this.IsActive = IsActive;
    }

    public double[] getL() {
        return l;
    }

    public void setL(double[] l) {
        this.l = l;
    }
}
