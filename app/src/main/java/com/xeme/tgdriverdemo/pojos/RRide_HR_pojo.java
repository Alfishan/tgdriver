package com.xeme.tgdriverdemo.pojos;

import java.util.Date;

/**
 * Created by root on 1/6/16.
 */

public class RRide_HR_pojo {

    public RRide_HR_pojo() {
    }

    public String getBookingID() {
        return BookingID;
    }

    public void setBookingID(String bookingId) {
        BookingID = bookingId;
    }

    String BookingID,PickUpAddress;
    Date PickUpTime, PickUpDate;
    String PackageType;
    Double Source_latlng[], User_latlng[];
    int RideTypeID, BookingTypeID, PackageTypeID;


    public RRide_HR_pojo(String BookingID,
                         int bookingTypeID,
                         int rideTypeID,
                         Double[] user_latlng,
                         Double[] source_latlng,
                         String PickUpAddress,
                         Date pickUpTime,
                         Date pickUpDate,
                         int packageTypeID,
                         String packageType) {
       this.BookingID=BookingID;
        BookingTypeID = bookingTypeID;
        RideTypeID = rideTypeID;
        User_latlng = user_latlng;
        Source_latlng = source_latlng;
        this.PickUpAddress = PickUpAddress;
        PickUpTime = pickUpTime;
        PickUpDate = pickUpDate;
        PackageTypeID = packageTypeID;
        PackageType = packageType;
    }

    public Date getPickUpTime() {
        return PickUpTime;
    }

    public void setPickUpTime(Date pickUpTime) {
        PickUpTime = pickUpTime;
    }

    public Date getPickUpDate() {
        return PickUpDate;
    }

    public void setPickUpDate(Date pickUpDate) {
        PickUpDate = pickUpDate;
    }

    public String getPackageType() {
        return PackageType;
    }

    public void setPackageType(String packageType) {
        PackageType = packageType;
    }

    public Double[] getSource_latlng() {
        return Source_latlng;
    }

    public void setSource_latlng(Double[] source_latlng) {
        Source_latlng = source_latlng;
    }

    public Double[] getUser_latlng() {
        return User_latlng;
    }

    public void setUser_latlng(Double[] user_latlng) {
        User_latlng = user_latlng;
    }

    public int getPackageTypeID() {
        return PackageTypeID;
    }

    public void setPackageTypeID(int packageTypeID) {
        PackageTypeID = packageTypeID;
    }

    public int getRideTypeID() {
        return RideTypeID;
    }

    public void setRideTypeID(int rideTypeID) {
        RideTypeID = rideTypeID;
    }

    public int getBookingTypeID() {
        return BookingTypeID;
    }

    public void setBookingTypeID(int bookingTypeID) {
        BookingTypeID = bookingTypeID;
    }

    public String getPickUpAddress() {
        return PickUpAddress;
    }

    public void setPickUpAddress(String pickUpAddress) {
        PickUpAddress = pickUpAddress;
    }
}

