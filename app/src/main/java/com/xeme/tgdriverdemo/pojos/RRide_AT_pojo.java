package com.xeme.tgdriverdemo.pojos;

import java.util.Date;

/**
 * Created by root on 1/6/16.
 */
public class RRide_AT_pojo{

    public String getBookingID() {
        return BookingID;
    }

    public void setBookingID(String bookingId) {
        BookingID = bookingId;
    }


    public String getEstimatedCost() {
        return EstimatedCost;
    }

    public void setEstimatedCost(String estimatedCost) {
        EstimatedCost = estimatedCost;
    }

    public String getEstimatedDistance() {
        return EstimatedDistance;
    }

    public void setEstimatedDistance(String estimatedDistance) {
        EstimatedDistance = estimatedDistance;
    }

    public String getEstimatedTime() {
        return EstimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        EstimatedTime = estimatedTime;
    }

    public String getPickUpAddress() {
        return PickUpAddress;
    }

    public void setPickUpAddress(String pickUpAddress) {
        PickUpAddress = pickUpAddress;
    }

    public String getDropAddress() {
        return DropAddress;
    }

    public void setDropAddress(String dropAddress) {
        DropAddress = dropAddress;
    }

    public Date getPickUpTime() {
        return PickUpTime;
    }

    public void setPickUpTime(Date pickUpTime) {
        PickUpTime = pickUpTime;
    }

    public Date getPickUpDate() {
        return PickUpDate;
    }

    public void setPickUpDate(Date pickUpDate) {
        PickUpDate = pickUpDate;
    }

    public Double[] getSource_latlng() {
        return Source_latlng;
    }

    public void setSource_latlng(Double[] source_latlng) {
        Source_latlng = source_latlng;
    }

    public Double[] getDestination_latlng() {
        return Destination_latlng;
    }

    public void setDestination_latlng(Double[] destination_latlng) {
        Destination_latlng = destination_latlng;
    }

    public Double[] getUser_latlng() {
        return User_latlng;
    }

    public void setUser_latlng(Double[] user_latlng) {
        User_latlng = user_latlng;
    }

    public int getRideTypeID() {
        return RideTypeID;
    }

    public void setRideTypeID(int rideTypeID) {
        RideTypeID = rideTypeID;
    }

    public int getBookingTypeID() {
        return BookingTypeID;
    }

    public void setBookingTypeID(int bookingTypeID) {
        BookingTypeID = bookingTypeID;
    }

    public boolean ISGoingToAirPort() {
        return ISGoingToAirPort;
    }

    public void setISGoingToAirPort(boolean ISGoingToAirPort) {
        this.ISGoingToAirPort = ISGoingToAirPort;
    }

    String EstimatedCost, BookingID,
            EstimatedDistance,
            EstimatedTime,PickUpAddress,DropAddress;
    Date PickUpTime, PickUpDate;
    Double Source_latlng[], Destination_latlng[], User_latlng[];
    int RideTypeID, BookingTypeID;
    boolean ISGoingToAirPort;
    public RRide_AT_pojo(String BookingID,
                         int BookingTypeID,
                         int RideTypeID,
                         Double[] User_latlng,
                         Double[] Source_latlng,
                         String PickUpAddress,
                         Double[] Destination_latlng,
                         String DropAddress,
                         Date PickUpTime,
                         Date PickUpDate,
                         String EstimatedCost,
                         String EstimatedDistance,
                         String EstimatedTime, boolean ISGoingToAirPort) {
        this.BookingID = BookingID;
        this.BookingTypeID = BookingTypeID;
        this.RideTypeID = RideTypeID;
        this.User_latlng = User_latlng;
        this.Source_latlng = Source_latlng;
        this.PickUpAddress = PickUpAddress;
        this.Destination_latlng = Destination_latlng;
        this.DropAddress = DropAddress;
        this.PickUpTime = PickUpTime;
        this.PickUpDate = PickUpDate;
        this.EstimatedCost = EstimatedCost;
        this.EstimatedDistance = EstimatedDistance;
        this.EstimatedTime = EstimatedTime;
        this.ISGoingToAirPort = ISGoingToAirPort;
    }
    public RRide_AT_pojo() {
    }

   /* public RRide_AT_pojo(Parcel in) {
        EstimatedCost = in.readString();
        EstimatedDistance = in.readString();
        EstimatedTime = in.readString();
        PickUpTime.setTime(in.readLong());
        PickUpDate.setTime(in.readLong());
        Source_latlng = in.readLongArray();
        Destination_latlng = destination_latlng;
        User_latlng = user_latlng;
        RideTypeID = rideTypeID;
        BookingTypeID = bookingTypeID;
    }*/


   /* @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(this.BookingTypeID);
        dest.writeInt(this.RideTypeID);
        dest.writeDoubleArray(this.User_latlng);
        dest.writeDoubleArray(this.Source_latlng);
        dest.writeDoubleArray(this.Destination_latlng);
        dest.writeLong(this.PickUpTime.getTime());
        dest.writeLong(this.PickUpDate.getTime());
        dest.writeString(this.EstimatedCost);
        dest.writeString(this.EstimatedDistance);
        dest.writeString(this.EstimatedTime);
       // dest.writeb(ISGoingToAirPort);

    }

    public static final Parcelable.Creator<RRide_AT_pojo> CREATOR = new Parcelable.Creator<RRide_AT_pojo>() {
        public RRide_AT_pojo createFromParcel(Parcel in) {
            return new RRide_AT_pojo(in);
        }

        public RRide_AT_pojo[] newArray(int size) {
            return new RRide_AT_pojo[size];

        }
    };
*/

}
