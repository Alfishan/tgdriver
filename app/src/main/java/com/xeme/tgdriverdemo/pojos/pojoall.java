package com.xeme.tgdriverdemo.pojos;

import java.util.Date;

/**
 * Created by root on 4/6/16.
 */
public class pojoall {

    String EstimatedCost;
    String BookingID;
    String EstimatedDistance;
    String EstimatedTime;
    String PickUpAddress;
    String DropAddress;
    String PackageType;
    Date PickUpTime;
    Date PickUpDate;
    double[] Source_latlng=new double[2];
    double[] Destination_latlng;
    double[] User_latlng;
    int RideTypeID;
    int BookingTypeID;
    int PackageTypeID;
    boolean ISGoingToAirPort;

    public pojoall() {
    }

    public pojoall(String estimatedCost, String bookingID, String estimatedDistance, String estimatedTime, String pickUpAddress, String dropAddress, String packageType, Date pickUpTime, Date pickUpDate, double[] source_latlng, double[] destination_latlng, double[] user_latlng, int rideTypeID, int bookingTypeID, int packageTypeID, boolean ISGoingToAirPort) {
        EstimatedCost = estimatedCost;
        BookingID = bookingID;
        EstimatedDistance = estimatedDistance;
        EstimatedTime = estimatedTime;
        PickUpAddress = pickUpAddress;
        DropAddress = dropAddress;
        PackageType = packageType;
        PickUpTime = pickUpTime;
        PickUpDate = pickUpDate;
        Source_latlng = source_latlng;
        Destination_latlng = destination_latlng;
        User_latlng = user_latlng;
        RideTypeID = rideTypeID;
        BookingTypeID = bookingTypeID;
        PackageTypeID = packageTypeID;
        this.ISGoingToAirPort = ISGoingToAirPort;
    }

    public String getEstimatedCost() {
        return EstimatedCost;
    }

    public void setEstimatedCost(String estimatedCost) {
        EstimatedCost = estimatedCost;
    }

    public boolean ISGoingToAirPort() {
        return ISGoingToAirPort;
    }

    public void setISGoingToAirPort(boolean ISGoingToAirPort) {
        this.ISGoingToAirPort = ISGoingToAirPort;
    }

    public int getPackageTypeID() {
        return PackageTypeID;
    }

    public void setPackageTypeID(int packageTypeID) {
        PackageTypeID = packageTypeID;
    }

    public int getBookingTypeID() {
        return BookingTypeID;
    }

    public void setBookingTypeID(int bookingTypeID) {
        BookingTypeID = bookingTypeID;
    }

    public int getRideTypeID() {
        return RideTypeID;
    }

    public void setRideTypeID(int rideTypeID) {
        RideTypeID = rideTypeID;
    }

    public double[] getUser_latlng() {
        return User_latlng;
    }

    public void setUser_latlng(double[] user_latlng) {
        User_latlng = user_latlng;
    }

    public double[] getDestination_latlng() {
        return Destination_latlng;
    }

    public void setDestination_latlng(double[] destination_latlng) {
        Destination_latlng = destination_latlng;
    }

    public double[] getSource_latlng() {
        return Source_latlng;
    }

    public void setSource_latlng(double[] source_latlng) {
        Source_latlng = source_latlng;
    }

    public Date getPickUpDate() {
        return PickUpDate;
    }

    public void setPickUpDate(Date pickUpDate) {
        PickUpDate = pickUpDate;
    }

    public Date getPickUpTime() {
        return PickUpTime;
    }

    public void setPickUpTime(Date pickUpTime) {
        PickUpTime = pickUpTime;
    }

    public String getPackageType() {
        return PackageType;
    }

    public void setPackageType(String packageType) {
        PackageType = packageType;
    }

    public String getDropAddress() {
        return DropAddress;
    }

    public void setDropAddress(String dropAddress) {
        DropAddress = dropAddress;
    }

    public String getPickUpAddress() {
        return PickUpAddress;
    }

    public void setPickUpAddress(String pickUpAddress) {
        PickUpAddress = pickUpAddress;
    }

    public String getEstimatedTime() {
        return EstimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        EstimatedTime = estimatedTime;
    }

    public String getEstimatedDistance() {
        return EstimatedDistance;
    }

    public void setEstimatedDistance(String estimatedDistance) {
        EstimatedDistance = estimatedDistance;
    }

    public String getBookingID() {
        return BookingID;
    }

    public void setBookingID(String bookingID) {
        BookingID = bookingID;
    }
}
