package com.xeme.tgdriverdemo.fragments;


import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.iid.FirebaseInstanceId;
import com.xeme.tgdriverdemo.MyFirebaseMessagingService;
import com.xeme.tgdriverdemo.R;
import com.xeme.tgdriverdemo.utils.Constants;
import com.xeme.tgdriverdemo.utils.gpslocation;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Frag_main#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Frag_main extends Fragment implements OnMapReadyCallback,LocationListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    GoogleMap map;
    MapView mapView;
    Context mContext;
    View V;
    Firebase ref ;
    private GeoFire geoFire;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    String Drive="Tommy Lee",token;
    int DriverUID=491992;
    public Frag_main() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Frag_main.
     */
    // TODO: Rename and change types and number of parameters
    public static Frag_main newInstance(String param1, String param2) {
        Frag_main fragment = new Frag_main();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext=getActivity();
        Firebase.setAndroidContext(mContext);
        EventBus.getDefault().register(this);
        this.ref = new Firebase(Constants.GEO_FIRE_Driver);
        this.geoFire = new GeoFire(new Firebase(Constants.GEO_FIRE_Driver).child("MyTexi5").child("Location"));

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        token=FirebaseInstanceId.getInstance().getToken();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        // Inflate the layout for this fragment
        V = inflater.inflate(R.layout.fragment_frag_main, container, false);

        mContext = getActivity().getBaseContext();
        mapView = (MapView) V.findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        try {
            MapsInitializer.initialize(this.getActivity());

        } catch (Exception e) {
            e.printStackTrace();
        }


        return V;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mapView != null) {
            mapView.onResume();
        }
        startLocationUpdates();

    }

    @Override
    public void onDestroy() {
        stopLocationUpdates();
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();

    }

    private void stopLocationUpdates(){
        try {

            gpslocation.mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    gpslocation.mGoogleApiClient);

            if (gpslocation.mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(
                        gpslocation.mGoogleApiClient, this);
            }
        } catch (SecurityException e) {

            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        stopLocationUpdates();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        try {
            startLocationUpdates();
            gpslocation.movetolastlocation(map);
            map.getUiSettings().setMyLocationButtonEnabled(true);
            map.setMyLocationEnabled(true);


            View locationButton = ((View) V.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            if (locationButton != null) {
                RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                // position on right bottom
                rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                rlp.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                rlp.setMargins(0, 0, 20, 0);
            }


        } catch (SecurityException e) {

            e.printStackTrace();
        }

       /* double l[]=new double[2];
        LatLng n=gpslocation.getlatlong();
        l[0]=n.latitude;
        l[1]=n.longitude;
        Texi mTexi=new Texi(1992,DriverUID,"Tommy Lee",true,l,token);
        ref.child("MyTexi5").setValue(mTexi);*/

    }

    private void startLocationUpdates() {
        try {

            gpslocation.mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    gpslocation.mGoogleApiClient);

            if (gpslocation.mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.requestLocationUpdates(
                        gpslocation.mGoogleApiClient, gpslocation.mLocationRequest, this);
            }
        } catch (SecurityException e) {

            e.printStackTrace();
        }

    }

    @Override
    public void onLocationChanged(Location location) {


        geoFire.setLocation(String.valueOf(DriverUID), new GeoLocation(location.getLatitude(), location.getLongitude()));

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String a=(String) dataSnapshot.child("MyTexi5").child("DriverName").getValue();
                Log.wtf(Constants.TAG,"DName="+ a);
               // Toast.makeText(mContext, "Data Changed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }


        });


    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(MyFirebaseMessagingService.Mydata1 m) {
        if (m.b.getInt("ForWho") == 3) {
            Log.wtf(Constants.TAG,"Ingreenrobot subcriber if "+"Hey Reach me at: eb"+ m.b.getString("lat")+ " ,"+m.b.getString("lng")+","+m.b.getInt("ForWho"));


            try {

                if (map != null)
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(m.b.getDouble("latd"),m.b.getDouble("lngd")), Constants.INITIAL_ZOOM_LEVEL));
            } catch (Exception e) {

                Log.wtf(Constants.TAG, "********Error******* :" + e);
            }

        }
    }

   /* @org.greenrobot.eventbus.Subscribe
    public void a(MyFirebaseMessagingService.Mydata1 m) {


        Log.wtf(Constants.TAG,"Ingreenrobot subcriber top  "+"Hey Reach me at: eb"+ m.b.getString("lat")+ " ,"+m.b.getString("lng")+","+m.b.getInt("ForWho"));

        if (m.b.getInt("ForWho") == 2) {
            Log.wtf(Constants.TAG,"Ingreenrobot subcriber if "+"Hey Reach me at: eb"+ m.b.getString("lat")+ " ,"+m.b.getString("lng"));
        }
        if (m.b.getInt("ForWho") == 3) {
            Log.wtf(Constants.TAG,"Ingreenrobot subcriber if "+"Hey Reach me at: eb"+ m.b.getString("lat")+ " ,"+m.b.getString("lng")+","+m.b.getInt("ForWho"));


            try {

                if (map != null)
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(m.b.getDouble("latd"),m.b.getDouble("lngd")), Constants.INITIAL_ZOOM_LEVEL));
            } catch (Exception e) {

                Log.wtf(Constants.TAG, "********Error******* :" + e);
            }

        }
    }*/
}
