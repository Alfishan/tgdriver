package com.xeme.tgdriverdemo.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xeme.tgdriverdemo.R;
import com.xeme.tgdriverdemo.pojos.RRide_AT_pojo;
import com.xeme.tgdriverdemo.pojos.RRide_HR_pojo;
import com.xeme.tgdriverdemo.pojos.RRide_pojo;

import java.text.SimpleDateFormat;
import java.util.List;


/**
 * Created by root on 2/6/16.
 */
public class at_rr_adapter extends RecyclerView.Adapter<at_rr_adapter.MyViewHolder>  {

    // Define listener member variable
    private static OnItemClickListener listener;
    int pos;
    int whichpojo;
    Context mContext;
    SimpleDateFormat formate_date = new SimpleDateFormat("dd-MM-yyyy");
    SimpleDateFormat format_time = new SimpleDateFormat("hh:mm a");
    private List<RRide_AT_pojo> pojo_AtList;
    private List<RRide_HR_pojo> pojo_HRList;
    private List<RRide_pojo> pojo_p2pList;

    public at_rr_adapter(List<RRide_pojo> pojo_p2pList, List<RRide_AT_pojo> pojo_AtList, List<RRide_HR_pojo> pojo_HRList, int a) {

        whichpojo = a;
        if (a == 1) {
            this.pojo_p2pList = pojo_p2pList;

        } else if (a == 2) {
            this.pojo_AtList = pojo_AtList;

        } else if (a == 3) {
            this.pojo_HRList = pojo_HRList;
        }


    }


    public at_rr_adapter(Context ctx) {
        this.mContext = ctx;
    }

    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_card, parent, false);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Log.wtf(Constants.TAG, "onClick: ");
            }
        });

        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        pos = holder.getAdapterPosition();

        if (whichpojo == 1) {
            RRide_pojo pojo = pojo_p2pList.get(position);
            holder.pickup_address.setText(pojo.getPickUpAddress());
            holder.pickup_date.setText(formate_date.format(pojo.getPickUpDate()));
            holder.pickup_time.setText(format_time.format(pojo.getPickUpTime()));
            holder.cdt_c.setText(pojo.getEstimatedCost());
            holder.cdt_d.setText(pojo.getEstimatedDistance());
            holder.cdt_t.setText(pojo.getEstimatedTime());
            holder.ll_package.setVisibility(View.GONE);
            holder.ll_airport.setVisibility(View.GONE);
            holder.ll_cdt.setVisibility(View.VISIBLE);

        } else if (whichpojo == 2) {
            RRide_AT_pojo at_pojo = pojo_AtList.get(position);
            holder.pickup_address.setText(at_pojo.getPickUpAddress());
            holder.pickup_date.setText(formate_date.format(at_pojo.getPickUpDate()));
            holder.pickup_time.setText(format_time.format(at_pojo.getPickUpTime()));
            holder.cdt_c.setText(at_pojo.getEstimatedCost());
            holder.cdt_d.setText(at_pojo.getEstimatedDistance());
            holder.cdt_t.setText(at_pojo.getEstimatedTime());
            holder.ll_airport.setVisibility(View.VISIBLE);
            holder.ll_cdt.setVisibility(View.VISIBLE);

            if (at_pojo.ISGoingToAirPort()) {

                holder.tv_airport.setText("Going To Airport");
            } else {
                holder.tv_airport.setText("Coming  From Airport");
            }
            holder.ll_package.setVisibility(View.GONE);


        } else if (whichpojo == 3) {
            RRide_HR_pojo pojo = pojo_HRList.get(position);
            holder.ll_package.setVisibility(View.VISIBLE);
            holder.ll_airport.setVisibility(View.GONE);

            holder.pickup_address.setText(pojo.getPickUpAddress());
            holder.pickup_date.setText(formate_date.format(pojo.getPickUpDate()));
            holder.pickup_time.setText(format_time.format(pojo.getPickUpTime()));
            holder.tv_package.setText(pojo.getPackageType());
            holder.ll_cdt.setVisibility(View.GONE);

            /*holder.line1.setText(pojo.getBookingID());
            holder.line2.setText(pojo.getPackageType());*/

        }


    }

    @Override
    public int getItemCount() {

        if (whichpojo == 1) {
            return pojo_p2pList.size();
        } else if (whichpojo == 2) {
            return pojo_AtList.size();
        } else {
            return pojo_HRList.size();
        }

    }

    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView cdt_c, cdt_d, cdt_t, pickup_address, pickup_date, pickup_time, tv_airport, tv_package;
        LinearLayout ll_airport, ll_package, ll_cdt;
        private Context context;

        public MyViewHolder(final View view) {
            super(view);
            cdt_c = (TextView) view.findViewById(R.id.cdt_c);
            cdt_d = (TextView) view.findViewById(R.id.cdt_d);
            cdt_t = (TextView) view.findViewById(R.id.cdt_t);
            pickup_address = (TextView) view.findViewById(R.id.tv_list_item_pickup);
            pickup_date = (TextView) view.findViewById(R.id.tv_list_item_pick_up_date);
            pickup_time = (TextView) view.findViewById(R.id.tv_list_item_pick_up_time);
            tv_airport = (TextView) view.findViewById(R.id.list_item_tv_airport);
            tv_package = (TextView) view.findViewById(R.id.list_item_tv_Package);
            ll_airport = (LinearLayout) view.findViewById(R.id.list_item_ll_airport);
            ll_package = (LinearLayout) view.findViewById(R.id.list_item_ll_package);
            ll_cdt = (LinearLayout) view.findViewById(R.id.list_item_ll_cdt);


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.onItemClick(itemView, getLayoutPosition());
                }
            });


        }
    }
}

